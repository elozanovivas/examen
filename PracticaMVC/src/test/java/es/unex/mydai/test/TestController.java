package es.unex.mydai.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.catalina.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.TestExecutionListeners;

import es.unex.mydai.persistence.vo.Grupo;
import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Publicacion;
import es.unex.mydai.persistence.vo.Users;
import es.unex.mydai.persistence.vo.Visitas;
import es.unex.mydai.service.GrupoService;
import es.unex.mydai.service.GrupoServiceImpl;
import es.unex.mydai.service.HashtagService;
import es.unex.mydai.service.HashtagServiceImpl;
import es.unex.mydai.service.MensajesService;
import es.unex.mydai.service.MensajesServiceImpl;
import es.unex.mydai.service.PublicacionService;
import es.unex.mydai.service.PublicacionServiceImpl;
import es.unex.mydai.service.UsersService;
import es.unex.mydai.service.UsersServiceImpl;
import es.unex.mydai.service.VisitaService;
import es.unex.mydai.service.VisitaServiceImpl;
import junit.framework.TestCase;


public class TestController extends TestCase {
	@Test
	public void test() {
		
		ApplicationContext context=new ClassPathXmlApplicationContext("file:src/main/webapp/WEB-INF/SpringContext.xml");
		UsersService usuarioService=(UsersService)context.getBean(UsersServiceImpl.class);
		
		Users u=new Users();
		u.setEmail("emilio@emilio.com");
		u.setNameUser("Emilio");
		u.setPass("123");

		Users u2=new Users();
		u2.setEmail("josemanuel@josemanuel.com");
		u2.setNameUser("Jose Manuel");
		u2.setPass("123");
		
		Users u3=new Users();
		u3.setEmail("rocio@rocio.com");
		u3.setNameUser("Rocio");
		u3.setPass("123");
		//Insertamos usuarios
		usuarioService.createUser(u);
		usuarioService.createUser(u2);
		usuarioService.createUser(u3);
		
		GrupoService grupoService=(GrupoService)context.getBean(GrupoServiceImpl.class);
		
		Grupo g= new Grupo();
		g.setNombreGrupo("CUM");
		List<Users> listaUsers= new ArrayList<Users>();
		listaUsers.add(u);
		listaUsers.add(u2);
		g.setUsuariosLista(listaUsers);
		java.util.Date fecha = new Date();
		g.setFechaCreacion(fecha);
		
		Grupo g2= new Grupo();
		g2.setNombreGrupo("Profesores");
		List<Users> listaUsers2= new ArrayList<Users>();
		listaUsers2.add(u3);
		listaUsers2.add(u2);
		g2.setUsuariosLista(listaUsers2);
		java.util.Date fecha2 = new Date();
		g2.setFechaCreacion(fecha2);
		
		//Insertamos grupos
		grupoService.createGrupo(g);
		grupoService.createGrupo(g2);
		
		HashtagService hs= (HashtagService)context.getBean(HashtagServiceImpl.class);
		Hashtags h=new Hashtags();
		h.setContenido("Metodologia y desarrollo de aplicaciones para internet");
		h.setUser(u);
		h.setHashtag("#MyDAI");
		
		Hashtags h2=new Hashtags();
		h2.setContenido("Centro Universitario de Merida");
		h2.setUser(u2);
		h2.setHashtag("#CUM");
		
		//Insertamos hashtags
		hs.createHashtag(h);
		hs.createHashtag(h2);
		
		VisitaService vs= (VisitaService)context.getBean(VisitaServiceImpl.class);
		Visitas v= new Visitas();
		v.setIdUsuarioVisitado(1);
		v.setIdUsuarioVisitante(2);
		
		Visitas v1= new Visitas();
		v1.setIdUsuarioVisitado(2);
		v1.setIdUsuarioVisitante(1);
		
		Visitas v2= new Visitas();
		v2.setIdUsuarioVisitado(3);
		v2.setIdUsuarioVisitante(1);
		
		Visitas v3= new Visitas();
		v3.setIdUsuarioVisitado(1);
		v3.setIdUsuarioVisitante(3);
		
		//Insertamos visita
		vs.createVisita(v);
		vs.createVisita(v1);
		vs.createVisita(v2);
		vs.createVisita(v3);
		
		PublicacionService ps=(PublicacionService)context.getBean(PublicacionServiceImpl.class);
		
		Publicacion p= new Publicacion();
		p.setIdUsuarioOrigen(u);
		p.setTexto("Examen 18 Enero");
		
		Publicacion p1= new Publicacion();
		p1.setIdUsuarioOrigen(u);
		p1.setTexto("Publicacion1");
		
		Publicacion p2= new Publicacion();
		p2.setIdUsuarioOrigen(u2);
		p2.setTexto("Examen 18 Enero");
		
		Publicacion p3= new Publicacion();
		p3.setIdUsuarioOrigen(u);
		p3.setTexto("Entrega realizada");
		
		//Insertamos publicacion
		ps.createPublicacion(p);
		ps.createPublicacion(p1);
		ps.createPublicacion(p2);
		ps.createPublicacion(p3);
		
		MensajesService ms= (MensajesService)context.getBean(MensajesServiceImpl.class);
		
		Mensajes m= new Mensajes();
		m.setIdUsuarioOrigen(u);
		m.setIdUsuarioDestino(2);
		m.setTexto("Esto es un mensaje privado");
		
		Mensajes m1= new Mensajes();
		m1.setIdUsuarioOrigen(u2);
		m1.setIdUsuarioDestino(1);
		m1.setTexto("Llega correctamente?");
		
		Mensajes m2= new Mensajes();
		m2.setIdUsuarioOrigen(u3);
		m2.setIdUsuarioDestino(1);
		m2.setTexto("�A que hora quedamos?");
		
		
		Mensajes m3= new Mensajes();
		m3.setIdUsuarioOrigen(u3);
		m3.setIdUsuarioDestino(1);
		m3.setTexto("Funciona correcto");
		
		//Insertamos mensaje privado
		ms.createMensaje(m);
		ms.createMensaje(m1);
		ms.createMensaje(m2);
		ms.createMensaje(m3);
		
		
	}
	
	
}
