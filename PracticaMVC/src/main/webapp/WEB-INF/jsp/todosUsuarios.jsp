<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista de usuarios</title>
</head>
<body>
<h2>Busca el usuario a visitar</h2>
<form:form modelAttribute="u" action="anadirVisita">
		<fieldset>
			<legend>Busca el email del usuario</legend>
			<label for="emai">Email:</label>
			<form:input path="email" type="text" name="email" id="email"
				size="30" maxlength="150" />
			
		</fieldset>
		<br />
		<br />
		<h2>
			<c:if test="${mensaje!=null}">${mensaje}</c:if>
		</h2>
		<input type="submit" name="Buscar" value="Buscar" />
		
	</form:form>
</body>
</html>