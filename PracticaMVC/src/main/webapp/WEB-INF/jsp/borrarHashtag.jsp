<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="es.unex.mydai.persistence.vo.Users"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Borrar Hashtag</title>
</head>
<body>
<form:form modelAttribute="h" action="borrarHashtag">
		<fieldset>
			<legend> Borrar Hashtag </legend>
			<label for="hashtag_">Introduce el id del hashtag a borrar</label>
			<form:input path="idHashtag" type="text" name="idHashtag" id="idHashtag" size="30" maxlength="150"  required="required"/>

		</fieldset>
		<br />
		<br />
		<input type="submit" name="Borrar hashtag" value="Borrar hashtag" />
		<input type="reset" name="Borrar Datos" value="Borrar Datos">
</form:form>	
</body>
</html>