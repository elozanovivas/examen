<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar Hashtag</title>
</head>
<body>
<form:form modelAttribute="hashtag" action="editarHashtag">
		<fieldset>
			<legend> Editar hashtag </legend>
			<label for="id_">Id:</label>
			<form:input path="idHashtag" type="text" name="idHashtag" id="idHashtag"
				size="30" maxlength="150" required="required"/>
			<label for="hashtag_">Hashtag</label>
			<form:input path="hashtag" type="text" name="hashtag" id="hashtag" value="#"
				size="30" maxlength="150" required="required"/>
			<label for="contenido_">Contenido</label>
			<form:input path="contenido" type="text" name="contenido" id="contenido"
				size="30" maxlength="150" required="required"/>
			<br />
		
		</fieldset>
		<br />
		<br />
		<input type="submit" name="Editar Hashtag" value="Editar Hashtag" />
		<input type="reset" name="Borrar Datos" value="Borrar Datos">
</form:form>	
</body>
</html>