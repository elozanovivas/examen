<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Menu</title>
</head>
<body>
<h2>Bienvenido <c:out value="${user.nameUser}"></c:out></h2>
<h2>Id usuario <c:out value="${user.idUsers}"></c:out></h2>
<ul>
	<li><spring:url value="/{idUsers}/mostrarUser" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Mostrar usuario</a></li>
	
	<li><spring:url value="/{idUsers}/editarUser" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Editar usuario</a></li>
	
		<li><spring:url value="/{idUsers}/borrarUser" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Borrar Usuario</a></li>
	
			<li><spring:url value="/{idUsers}/crearHashtag" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Crear hashtag</a></li>
	
				<li><spring:url value="/{idUsers}/editarHashtag" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Editar hashtag</a></li>
	
					<li><spring:url value="/{idUsers}/mostrarHashtag" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Mostrar hashtags</a></li>
	
	<li><spring:url value="/{idUsers}/borrarHashtag" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Eliminar hashtags</a></li>

		<li><spring:url value="/{idUsers}/anadirGrupo" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Nuevo grupo</a></li>
	
			<li><spring:url value="/{idUsers}/mostrarGrupo" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Grupos existentes</a></li>
	
			<li><spring:url value="/{idUsers}/editarGrupo" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Editar grupo</a></li>
	
<li><spring:url value="/{idUsers}/borrarGrupo" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Borrar grupo</a></li>
	
	<li><spring:url value="/{idUsers}/unirseGrupo" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Unirse a nuevo grupo</a></li>
		
<li><spring:url value="/{idUsers}/anadirVisita" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Ver perfil</a></li>
	
	<li><spring:url value="/{idUsers}/mostrarVisita" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Ver visitas en tu perfil</a></li>
	
	<li><spring:url value="/{idUsers}/anadirMensaje" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Enviar mensaje privado</a></li>
	
		<li><spring:url value="/{idUsers}/mostrarMensaje" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Ver mensajes recibidos</a></li>
	
			<li><spring:url value="/{idUsers}/anadirPublicacion" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Crear mensaje publico</a></li>
	
<li><spring:url value="/{idUsers}/mostrarPublicaciones" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Ver mensajes p�blicos</a></li>
	<%-- 
		<li><spring:url value="/{idUsers}/anadirChargingStation" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Nueva estaciones de recarga</a></li>	
	
	<li><spring:url value="/mostrarChargingStations" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Mostrar estaciones de recarga</a></li>	
	

	

	
	<li><spring:url value="/mostrarChargingSystems" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Mostrar sistemas de recarga</a></li>
	
	<li><spring:url value="/{idUsers}/anadirReserva" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Nueva reserva</a></li>
	
		<li><spring:url value="/{idUsers}/anadirChargingSystem" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Nuevo sistema de recarga</a></li>
	
			<li><spring:url value="/{idUsers}/borrarChargingSystem" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Borrar sistema de recarga</a></li>
	
		<li><spring:url value="/{idUsers}/anadirCoche" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">A�adir coche del usuario</a></li>
	
	<li><spring:url value="/{idUsers}/borrarCoche" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Borrar coche del usuario</a></li>
	
		<li><spring:url value="/{idUsers}/borrarChargingStation" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Borrar estacion de recarga</a></li>
	
		<li><spring:url value="/{idUsers}/recambioBateria" var="usuarioUrl">
	<spring:param name="idUsers" value="${user.idUsers}"></spring:param></spring:url>
	<a href="${fn:escapeXml(usuarioUrl) }">Recambio de bateria</a></li>
	--%>
		<li><spring:url value="/login" var="usuarioUrl2">
	</spring:url> 
	<a href="${fn:escapeXml(usuarioUrl2) }">Salir</a>
	</li>	
</ul>
<h2><c:out value="${mensaje}"></c:out></h2>
</body>
</html>