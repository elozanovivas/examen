<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buzon mensajes</title>
</head>
<body>
<h2>Buzon de mensajes</h2>

	<c:if test="${mensaje!=null}">${mensaje}</c:if>
<table border="1">
	<tr>
		<th>Contenido</th>
		<th>Usuario remitente</th>
	</tr>
 	<c:forEach var="mensajes" items="${mensajes}">
		<tr>
			<td><c:out value="${mensajes.texto}"/></td>
			<td><c:out value="${mensajes.idUsuarioOrigen.nameUser}"/></td>
		</tr>
	</c:forEach>
</table>

</body>
</html>