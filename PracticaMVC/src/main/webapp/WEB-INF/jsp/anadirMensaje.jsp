<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="es.unex.mydai.persistence.vo.Users"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nuevo mensaje privado</title>
</head>
<body>
<form:form modelAttribute="m" action="anadirMensaje">
		<fieldset>
			<legend> Crear mensaje </legend>
			<label for="contenido_">Contenido</label>
			<form:input path="texto" type="text" name="texto" id="texto" size="30" maxlength="150" required="required"/>
			<label for="idUsuarioDestino_">Destinatario</label>
			<form:input path="idUsuarioDestino" type="text" name="idUsuarioDestino" id="idUsuarioDestino" size="30" maxlength="150" required="required"/>
			<br />
		
		</fieldset>
		<br />
		<br />
		<input type="submit" name="Enviar mensaje" value="Enviar mensaje" />
		<input type="reset" name="Borrar Datos" value="Borrar Datos">
</form:form>	
</body>
</html>