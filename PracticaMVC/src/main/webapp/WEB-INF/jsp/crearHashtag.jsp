<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="es.unex.mydai.persistence.vo.Users"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Crear Hashtag</title>
</head>
<body>
<form:form modelAttribute="h" action="crearHashtag">
		<fieldset>
			<legend> Crear Hashtag </legend>
			<label for="hashtag_">Hashtag</label>
			<form:input path="hashtag" type="text" name="hashtag" id="hashtag" size="30" maxlength="150" value="#" required="required"/>
			<label for="contenido_">contenido</label>
			<form:input path="contenido" type="text" name="contenido" id="contenido" size="30" maxlength="150" required="required"/>
			<br />
		
		</fieldset>
		<br />
		<br />
		<input type="submit" name="Anadir hashtag" value="A�adir hashtag" />
		<input type="reset" name="Borrar Datos" value="Borrar Datos">
</form:form>	
</body>
</html>