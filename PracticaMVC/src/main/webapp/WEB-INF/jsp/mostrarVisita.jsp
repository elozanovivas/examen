<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Visitas</title>
</head>
<body>
<h2>Visitas a tu perfil</h2>

	<c:if test="${mensaje!=null}">${mensaje}</c:if>
<table border="1">
	<tr>
		<th>Usuarios visitante</th>
	
	</tr>
 	<c:forEach var="users" items="${users}">
		<tr>
			<td><c:out value="${users.nameUser}"/></td>	
		</tr>
	</c:forEach>
</table>

</body>
</html>