package es.unex.mydai.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Users;
import es.unex.mydai.service.HashtagService;
import es.unex.mydai.service.UsersService;

@Controller
public class HashtagController {

	private final HashtagService hashtagService;
	private final UsersService usersService;
	@Autowired
	public HashtagController(HashtagService hashtagService,UsersService usersService) {
		this.hashtagService = hashtagService;
		this.usersService = usersService;
	}
	
	@RequestMapping(value = "/{idUsers}/crearHashtag", method = RequestMethod.GET)
	public String anadirUsuario(ModelMap model) {
	Users u = new Users();
	model.addAttribute("u", u);
	model.addAttribute("user", u);
	Hashtags h= new Hashtags();
	model.addAttribute("h",h);
		return "crearHashtag";
	}

	@RequestMapping(value = "/{idUsers}/crearHashtag", method = RequestMethod.POST)
	public String insertarUsuario(ModelMap model, @ModelAttribute("h") Hashtags h,@PathVariable("idUsers") long id) {
		if (!h.getHashtag().equals("") && !h.getContenido().equals("")) {
			try {
			Users usuario = this.usersService.findUserById(id);
			h.setUser(usuario);
			model.addAttribute("user", usuario);
						this.hashtagService.createHashtag(h);
				model.addAttribute("mensaje", "Hashtag creado correctamente");
				return "menu";
			} catch (Exception e) {
				model.addAttribute("mensaje", "Problema creando el hashtag");
				return "menu";
			}

		}
		model.addAttribute("mensaje", "Los campos deben estar completos");
		return "menu";
	}
	@RequestMapping(value = "/{idUsers}/editarHashtag", method = RequestMethod.GET)
	public String editarHashtag(ModelMap model,@PathVariable("idUsers") long id) {
	Users u = new Users();
	Users usuario = this.usersService.findUserById(id);
	ArrayList<Hashtags> h1= this.hashtagService.busqueda(id);
	model.addAttribute("lista", h1);
	model.addAttribute("u", u);
	model.addAttribute("user", u);
	Hashtags h= new Hashtags();
	
	model.addAttribute("hashtag",h);
		return "editarHashtag";
	}
	
	@RequestMapping(value = "/{idUsers}/editarHashtag", method = RequestMethod.POST)
	public String editarHashtag(ModelMap model, @ModelAttribute("hashtag") Hashtags h,@PathVariable("idUsers") long id) {
		Users usuario = this.usersService.findUserById(id);
		model.addAttribute("user", usuario);
	
		
		try {
		long id_hashtag=h.getIdHashtag();
		Hashtags hashtag=this.hashtagService.findById(id_hashtag);
		hashtag.setHashtag(h.getHashtag());
		hashtag.setContenido(h.getContenido());
		this.hashtagService.updateHashtag(hashtag);
		
		model.addAttribute("mensaje", "El hashtag fue editado correctamente");
		return "menu";
		
		
		}catch (Exception e) {
			model.addAttribute("mensaje", "Problema editando el hashtag");
			return "menu";		
			}
	
	}
	@RequestMapping(value = "/{idUsers}/mostrarHashtag", method = RequestMethod.GET)
	public String mostrarHashtag(ModelMap model,@PathVariable("idUsers") long id) {
	Users u = new Users();
	
	Users usuario = this.usersService.findUserById(id);
	List <Hashtags> h1= this.hashtagService.todosHashtags();
	model.addAttribute("hashtags", h1);
	model.addAttribute("u", u);
	model.addAttribute("user", u);
	Hashtags h= new Hashtags();
	
	model.addAttribute("hashtag",h);
		return "mostrarHashtags";
	}
	@RequestMapping(value = "/{idUsers}/borrarHashtag", method = RequestMethod.GET)
	public String eliminarHashtag(ModelMap model,@PathVariable("idUsers") long id) {
	Users u = new Users();

	Hashtags h= new Hashtags();
	
	model.addAttribute("user", u);
	model.addAttribute("h", h);
		return "borrarHashtag";
	}
	
	@RequestMapping(value = "/{idUsers}/borrarHashtag", method = RequestMethod.POST)
	public String borrarHashtag(ModelMap model, @ModelAttribute("hashtag") Hashtags h,@PathVariable("idUsers") long id) {
	Users usuario = this.usersService.findUserById(id);
	model.addAttribute("user", usuario);

	Hashtags hasht=this.hashtagService.findById(h.getIdHashtag());
	if(hasht.getUser().getIdUsers()==usuario.getIdUsers()) {
		this.hashtagService.deleteHashtag(hasht);
		model.addAttribute("mensaje", "Eliminado correctamente");
		return "menu";
	}else { 
		model.addAttribute("mensaje", "El hashtag seleccionado no pertenece a este usuario, por lo tanto no se puede eliminar");
		return "menu";}
	}


}
