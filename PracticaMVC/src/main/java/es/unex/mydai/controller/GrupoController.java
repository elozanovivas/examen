package es.unex.mydai.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.mydai.persistence.vo.Grupo;
import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Users;
import es.unex.mydai.service.GrupoService;
import es.unex.mydai.service.UsersService;

@Controller
public class GrupoController {

	private final GrupoService grupoService;
	private final UsersService usersService;
	@Autowired
	public GrupoController(GrupoService grupoService ,UsersService usersService) {
		this.grupoService = grupoService;
		this.usersService = usersService;
	}
	
	@RequestMapping(value = "/{idUsers}/anadirGrupo", method = RequestMethod.GET)
	public String anadirGrupo(ModelMap model) {
	Users u = new Users();
	model.addAttribute("u", u);
	model.addAttribute("user", u);
	Grupo g= new Grupo();
	model.addAttribute("g",g);
		return "anadirGrupo";
	}

	@RequestMapping(value = "/{idUsers}/anadirGrupo", method = RequestMethod.POST)
	public String anadirGrupo(ModelMap model, @ModelAttribute("g") Grupo g, @PathVariable("idUsers") long id) {
		if (!g.getNombreGrupo().equals("")) {
			try {
			Users usuario = this.usersService.findUserById(id);
			List<Users> lista= new ArrayList<Users>();
			lista.add(usuario);
			g.setUsuariosLista(lista);
			//g.setCreador(usuario);
			model.addAttribute("user", usuario);
			java.util.Date fecha = new Date();
			g.setFechaCreacion(fecha);
			this.grupoService.createGrupo(g);
				model.addAttribute("mensaje", "Grupo creado correctamente");
				return "menu";
			} catch (Exception e) {
				model.addAttribute("mensaje", "Problema creando el grupo");
				return "menu";
			}

		}
		model.addAttribute("mensaje", "Los campos deben estar completos");
		return "menu";
	}

	@RequestMapping(value = "/{idUsers}/mostrarGrupo", method = RequestMethod.GET)
	public String mostrarGrupo(ModelMap model,@PathVariable("idUsers") long id) {
	Users u = new Users();
	
	Users usuario = this.usersService.findUserById(id);
	List <Grupo> g= this.grupoService.todosGrupos();
	model.addAttribute("grupo", g);
	model.addAttribute("u", u);
	model.addAttribute("user", u);
		return "mostrarGrupo";
	}
	
	@RequestMapping(value = "/{idUsers}/editarGrupo", method = RequestMethod.GET)
	public String editarGrupo(ModelMap model) {
	Users u = new Users();
	model.addAttribute("u", u);
	model.addAttribute("user", u);
	Grupo g= new Grupo();
	model.addAttribute("grupo",g);
		return "editarGrupo";
	}

	@RequestMapping(value = "/{idUsers}/editarGrupo", method = RequestMethod.POST)
	public String editarGrupo(ModelMap model, @ModelAttribute("grupo") Grupo g, @PathVariable("idUsers") long id) {
		if (!g.getNombreGrupo().equals("")) {
			boolean valido=false;
			try {
			Users usuario = this.usersService.findUserById(id);
			model.addAttribute("user", usuario);

			Grupo grupo=this.grupoService.findGrupoById(g.getIdGrupo());
			for(int i=0;i<grupo.getUsuariosLista().size();i++) {
				if(grupo.getUsuariosLista().get(i).getIdUsers()==usuario.getIdUsers()) {
					valido=true;
				}

				}if(valido==true) {grupo.setNombreGrupo(g.getNombreGrupo());
					this.grupoService.updateGrupo(grupo);
					model.addAttribute("mensaje", "Grupo modificado correctamente");

				}else {	
					model.addAttribute("mensaje", "No puede actualizar un grupo del que no es miembro");

				}
				return "menu";
			} catch (Exception e) {
				model.addAttribute("mensaje", "Problema creando el grupo");
				return "menu";
			}

		}
		model.addAttribute("mensaje", "Los campos deben estar completos");
		return "menu";
	}

	@RequestMapping(value = "/{idUsers}/borrarGrupo", method = RequestMethod.GET)
	public String borrarGrupo(ModelMap model) {
	Users u = new Users();
	model.addAttribute("u", u);
	model.addAttribute("user", u);
	Grupo g= new Grupo();
	model.addAttribute("grupo",g);
		return "borrarGrupo";
	}

	@RequestMapping(value = "/{idUsers}/borrarGrupo", method = RequestMethod.POST)
	public String borrarGrupo(ModelMap model, @ModelAttribute("grupo") Grupo g, @PathVariable("idUsers") long id) {
		Users u = this.usersService.findUserById(id);
		model.addAttribute("user", u);
		if (!(g.getIdGrupo()==0)) {
			boolean valido=false;
			try {
			Users usuario = this.usersService.findUserById(id);
			Grupo grupo=this.grupoService.findGrupoById(g.getIdGrupo());
			for(int i=0;i<grupo.getUsuariosLista().size();i++) {
				if(grupo.getUsuariosLista().get(i).getIdUsers()==usuario.getIdUsers()) {
					valido=true;
				}

				}if(valido==true) {grupo.setNombreGrupo(g.getNombreGrupo());
					this.grupoService.deleteGrupo(grupo);
					model.addAttribute("mensaje", "Grupo borrado correctamente");

				}else {	
					model.addAttribute("mensaje", "No puede borrar un grupo del que no es miembro");

				}
				return "menu";
			} catch (Exception e) {
				model.addAttribute("mensaje", "Problema creando el grupo");
				return "menu";
			}

		}
		model.addAttribute("mensaje", "Los campos deben estar completos");
		return "menu";
	}
		
	 @RequestMapping(value = "/{idUsers}/unirseGrupo", method = RequestMethod.GET)
	public String unirseGrupo(ModelMap model) {
	Users u = new Users();
	model.addAttribute("u", u);
	model.addAttribute("user", u);
	Grupo g= new Grupo();
	model.addAttribute("grupo",g);
		return "unirseGrupo";
	}

	@RequestMapping(value = "/{idUsers}/unirseGrupo", method = RequestMethod.POST)
	public String unirseGrupo(ModelMap model, @ModelAttribute("grupo") Grupo g, @PathVariable("idUsers") long id) {
		Users us = this.usersService.findUserById(id);
		model.addAttribute("user", us);
		if (!(g.getIdGrupo()==0)) {
			boolean valido=true;
			try {
				System.out.println("PAra aqui1");
			Users usuario = this.usersService.findUserById(id);
			Grupo grupo=this.grupoService.findGrupoById(g.getIdGrupo());
			List<Users> lUsuarios =grupo.getUsuariosLista();
			
			for(int i=0;i<grupo.getUsuariosLista().size();i++) {
				if(grupo.getUsuariosLista().get(i).getIdUsers()==usuario.getIdUsers()) {
					valido=false;
				}

				}if(valido==true) {
					lUsuarios.add(usuario);
					grupo.setUsuariosLista(lUsuarios);
					this.grupoService.updateGrupo(grupo);
					model.addAttribute("mensaje", "Grupo a�adido a tu perfil correctamente");

				}else {
					model.addAttribute("mensaje", "Ya eres miembro del grupo asi que no se te puede volver a a�adir");

				}
				return "menu";
			} catch (Exception e) {
				model.addAttribute("mensaje", "Problema creando el grupo");
				return "menu";
			}

		}
		model.addAttribute("mensaje", "Los campos deben estar completos");
		return "menu";
	}	
	  
	
	
	
	
	
	
	

}
