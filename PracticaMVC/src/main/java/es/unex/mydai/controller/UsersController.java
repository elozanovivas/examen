package es.unex.mydai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Users;
import es.unex.mydai.service.MensajesService;
import es.unex.mydai.service.UsersService;

@Controller
public class UsersController {

	private final UsersService usersService;
	private final MensajesService mensajeService;
	
	@Autowired
	public UsersController(UsersService usersService,MensajesService mensajeService) {
		this.usersService = usersService;
		this.mensajeService= mensajeService;
	}
	
	@RequestMapping(value = "/anadirUsuario", method = RequestMethod.GET)
	public String anadirUsuario(ModelMap model) {
		Users u = new Users();
		model.addAttribute("u", u);
		return "anadirUser";
	}

	@RequestMapping(value = "/anadirUsuario", method = RequestMethod.POST)
	public String insertarUsuario(ModelMap model, @ModelAttribute("u") Users user) {
		if (!user.getEmail().equals("") && !user.getNameUser().equals("") && !user.getPass().equals("")) {
			try {
				Users aux = this.usersService.createUser(user);
				model.addAttribute("user", aux);
				return "menu";
			} catch (Exception e) {
				model.addAttribute("mensaje", "El usuario ya existe");
				return "anadirUser";
			}

		}
		model.addAttribute("mensaje", "Los campos deben estar completos");
		return "anadirUser";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model, @ModelAttribute("usuario") Users usuario) {
		Users u = new Users();
		model.addAttribute("usuario", u);
		
		return "login";
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String inicioSesion(ModelMap model, @ModelAttribute("usuario") Users users) {

		Users u = this.usersService.findUserbyname(users.getEmail());
		Users login = this.usersService.loguearse(u);

		if (login.getEmail().equals(users.getEmail()) && login.getPass().equals(users.getPass())) {
			model.addAttribute("user", login);
			model.addAttribute("id", login.getIdUsers());
			return "menu";
		} else {
			model.addAttribute("mensaje", "Usuario y contraseņa incorrecto");
			return "login";
		}
	}
	@RequestMapping(value = "/{idUsers}/borrarUser", method = RequestMethod.GET)
	public String borrarUsuario(@PathVariable("idUsers") long idUsers, ModelMap model) {
		Users u = this.usersService.findUserById(idUsers);
		model.addAttribute("user", u);
		return "borrarUser";
	}

	@RequestMapping(value = "/{idUsers}/borrarUser", method = RequestMethod.POST)
	public String borrarUsuario(@PathVariable("idUsers") long idUsers, ModelMap model,
			@ModelAttribute("e") Users usuario) {

		Users user = this.usersService.findUserById(idUsers);
		this.usersService.deleteUser(user);
		model.addAttribute("user", "a");
		model.addAttribute("id", "a");
		model.addAttribute("mensaje", "Usuario y contraseņa incorrecto");
		return "login";
	}

	@RequestMapping(value = "/{idUsers}/editarUser", method = RequestMethod.GET)
	public String editarUsuario(@PathVariable("idUsers") long idUsers, ModelMap model) {
		Users u = this.usersService.findUserById(idUsers);
		model.addAttribute("user", u);
		return "editarUser";
	}

	@RequestMapping(value = "/{idUsers}/editarUser", method = RequestMethod.POST)
	public String editarUsuario(@PathVariable("idUsers") long idUsers, ModelMap model,
			@ModelAttribute("e") Users usuario) {

		Users usuarioUpdate = this.usersService.findUserById(idUsers);
		usuarioUpdate.setNameUser(usuario.getNameUser());
		usuarioUpdate.setPass(usuario.getPass());

		Users u = this.usersService.updateUser(usuarioUpdate);
		model.addAttribute("user", u);
		return "menu";
	}
	
	
	@RequestMapping(value = "/{idUsers}/mostrarUser", method = RequestMethod.GET)
	public String mostrarUser(@PathVariable("idUsers") long idUsers, ModelMap model) {

		List<Users>listaUsuarios = this.usersService.todosUsuarios();
		model.addAttribute("users", listaUsuarios);
		
		return "mostrarUsuarios";
	}

	

}
