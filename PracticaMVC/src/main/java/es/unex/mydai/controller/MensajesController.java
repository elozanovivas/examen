package es.unex.mydai.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Users;
import es.unex.mydai.persistence.vo.Visitas;
import es.unex.mydai.service.HashtagService;
import es.unex.mydai.service.MensajesService;
import es.unex.mydai.service.UsersService;
import es.unex.mydai.service.VisitaService;

@Controller
public class MensajesController {

	private final MensajesService mensajesService;
	private final UsersService usersService;
	
	@Autowired
	public MensajesController(MensajesService mensajesService,UsersService usersService) {
		this.mensajesService = mensajesService;
		this.usersService = usersService;
	}
	
	@RequestMapping(value = "/{idUsers}/anadirMensaje", method = RequestMethod.GET)
	public String anadirUsuario(ModelMap model) {
		Users u = new Users();
		model.addAttribute("u", u);
		model.addAttribute("usuario", u);
		Mensajes m= new Mensajes();
		model.addAttribute("m",m);
		return "anadirMensaje";
	}

	@RequestMapping(value = "/{idUsers}/anadirMensaje", method = RequestMethod.POST)
	public String insertarUsuario(ModelMap model, @ModelAttribute("m") Mensajes m,@PathVariable("idUsers") long id) {
		Users us = this.usersService.findUserById(id);
		model.addAttribute("user", us);
			try {	
				Users usuario = this.usersService.findUserById(id);
				m.setIdUsuarioOrigen(usuario);
				m.setIdUsuarioDestino(m.getIdUsuarioDestino());
				this.mensajesService.createMensaje(m);

				model.addAttribute("u", id);
		 	
		 return "menu";

			} catch (Exception e) {
				model.addAttribute("mensaje", "Problema enviando el mensaje");
				return "menu";
}

	}
	
	@RequestMapping(value = "/{idUsers}/mostrarMensaje", method = RequestMethod.GET)
	public String mostrarMensaje(ModelMap model,@PathVariable("idUsers") long id) {
		Users u = new Users();
		model.addAttribute("u", u);
		model.addAttribute("usuario", u);
		Mensajes m= new Mensajes();
		List<Mensajes> listaMensajes=this.mensajesService.busqueda(id);
		
		model.addAttribute("mensajes",listaMensajes);
		
		return "mostrarMensaje";
	}


	


}
