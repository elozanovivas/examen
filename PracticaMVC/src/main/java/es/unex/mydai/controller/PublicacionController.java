package es.unex.mydai.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Publicacion;
import es.unex.mydai.persistence.vo.Users;
import es.unex.mydai.persistence.vo.Visitas;
import es.unex.mydai.service.HashtagService;
import es.unex.mydai.service.MensajesService;
import es.unex.mydai.service.PublicacionService;
import es.unex.mydai.service.UsersService;
import es.unex.mydai.service.VisitaService;

@Controller
public class PublicacionController {

	private final PublicacionService publicacionService;
	private final UsersService usersService;
	
	@Autowired
	public PublicacionController(PublicacionService publicacionService,UsersService usersService) {
		this.publicacionService = publicacionService;
		this.usersService = usersService;
	}
	
	@RequestMapping(value = "/{idUsers}/anadirPublicacion", method = RequestMethod.GET)
	public String anadirPublicacion(ModelMap model) {
		Users u = new Users();
		model.addAttribute("u", u);
		model.addAttribute("usuario", u);
		Publicacion p= new Publicacion();
		model.addAttribute("p",p);
		return "anadirPublicacion";
	}

	@RequestMapping(value = "/{idUsers}/anadirPublicacion", method = RequestMethod.POST)
	public String anadirPublicacion(ModelMap model, @ModelAttribute("p") Publicacion p,@PathVariable("idUsers") long id) {
		Users us = this.usersService.findUserById(id);
		model.addAttribute("user", us);
			try {	
				Users usuario = this.usersService.findUserById(id);
				p.setIdUsuarioOrigen(usuario);
				
				this.publicacionService.createPublicacion(p);
				model.addAttribute("mensaje", "Mensaje publicado correctamente");
				//model.addAttribute("p", id);
		 	
		 return "menu";

			} catch (Exception e) {
				model.addAttribute("mensaje", "Fallo durante la publicación");
				return "menu";
}

	}
	
	@RequestMapping(value = "/{idUsers}/mostrarPublicaciones", method = RequestMethod.GET)
	public String mostrarMensaje(ModelMap model,@PathVariable("idUsers") long id) {
		Users u = new Users();
		model.addAttribute("u", u);
		model.addAttribute("usuario", u);
		Publicacion publicacion=new Publicacion();
		List<Publicacion> listaPublicaciones=this.publicacionService.todasPublicaciones();
		
		model.addAttribute("publicacion",listaPublicaciones);
		
		return "mostrarPublicaciones";
	}


	


}
