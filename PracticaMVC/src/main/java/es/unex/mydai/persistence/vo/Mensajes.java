package es.unex.mydai.persistence.vo;


import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
@Inheritance
public class Mensajes extends Publicacion{

	@Column
private long idUsuarioDestino;

	public Mensajes(long idUsuarioDestino) {
		super();
		this.idUsuarioDestino = idUsuarioDestino;
	}

	public Mensajes() {
		super();
	}


	@Override
	public String toString() {
		return "Mensajes [idUsuarioDestino=" + idUsuarioDestino + ", getIdMensaje()=" + getIdMensaje()
				+ ", getIdUsuarioOrigen()=" + getIdUsuarioOrigen() + ", getTexto()=" + getTexto() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

	public long getIdUsuarioDestino() {
		return idUsuarioDestino;
	}

	public void setIdUsuarioDestino(long idUsuarioDestino) {
		this.idUsuarioDestino = idUsuarioDestino;
	}
	
	
	
//	@ManyToOne
//	@JoinColumn(name="user_id",insertable = false, updatable=false)
//	private Users idUsuarioDestino;
//	
//	public Mensajes() {
//		super();
//	}
//
//	public Mensajes(Users idUsuarioDestino) {
//		super();
//	
//		this.idUsuarioDestino = idUsuarioDestino;
//	}
//
//	
//	public Users getIdUsuarioDestino() {
//		return idUsuarioDestino;
//	}
//
//	public void setIdUsuarioDestino(Users idUsuarioDestino) {
//		this.idUsuarioDestino = idUsuarioDestino;
//	}
//
//	@Override
//	public String toString() {
//		return "Mensajes [idUsuarioDestino=" + idUsuarioDestino + "]";
//	}
//
//	


	
}
