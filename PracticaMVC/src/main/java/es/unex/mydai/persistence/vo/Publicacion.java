package es.unex.mydai.persistence.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table (name="PUBLICACION")
public class Publicacion implements Serializable {
	

	private static final long serialVersionUID =1L;
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private long idMensaje;
	
	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn (name="user_id")
	private Users idUsuarioOrigen;
//	
//	@ManyToOne
//	private Users idUsuarioDestino;
//	
	@Column(name= "texto")
	private String texto;
	
//	@Column(name="publico")
//	private boolean publico;

	
	
	public Publicacion() {
		super();
	}

	public Publicacion(long idMensaje, 
			Users idUsuarioOrigen,
			//Users idUsuarioDestino,
			String texto) {
		super();
		this.idMensaje = idMensaje;
		this.idUsuarioOrigen = idUsuarioOrigen;
//		this.idUsuarioDestino = idUsuarioDestino;
		this.texto = texto;
		//this.publico = publico;
	}

	public long getIdMensaje() {
		return idMensaje;
	}

	public void setIdMensaje(long idMensaje) {
		this.idMensaje = idMensaje;
	}


//	public Users getIdUsuarioDestino() {
//		return idUsuarioDestino;
//	}
//
//	public void setIdUsuarioDestino(Users idUsuarioDestino) {
//		this.idUsuarioDestino = idUsuarioDestino;
//	}

	public Users getIdUsuarioOrigen() {
		return idUsuarioOrigen;
	}

	public void setIdUsuarioOrigen(Users idUsuarioOrigen) {
		this.idUsuarioOrigen = idUsuarioOrigen;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

//	public boolean isPublico() {
//		return publico;
//	}
//
//	public void setPublico(boolean publico) {
//		this.publico = publico;
//	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Publicacion [idMensaje=" + idMensaje + ", idUsuarioOrigen=" + idUsuarioOrigen + ", texto=" + texto
				+ "]";
	}
			
	
		
	
}
