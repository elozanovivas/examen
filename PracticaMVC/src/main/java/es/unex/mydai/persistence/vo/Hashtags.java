package es.unex.mydai.persistence.vo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="HASHTAGS")
public class Hashtags {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private long idHashtag;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private Users user;
	
	@Column (name="hashtag")
	private String hashtag;
	
	@Column(name="contenido")
	private String contenido;

	public Hashtags() {
		super();
	}

	public Hashtags(long idHashtag, Users user, String hashtag,
			String contenido) {
		super();
		this.idHashtag = idHashtag;
		this.user = user;
		this.hashtag = hashtag;
		this.contenido = contenido;
	}

	public long getIdHashtag() {
		return idHashtag;
	}

	public void setIdHashtag(long idHashtag) {
		this.idHashtag = idHashtag;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getHashtag() {
		return hashtag;
	}

	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Hashtags [idHashtag=" + idHashtag + ", user=" + user
				+ ", hashtag=" + hashtag + ", contenido=" + contenido
				+ ", fecha=" + "]";
	}
	
	
}
