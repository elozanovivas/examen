package es.unex.mydai.persistence.vo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table (name= "USERS")
public class Users implements Serializable{


	private static final long serialVersionUID =1L;
	@Id
	@Column(name="idusers")
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private long idUsers;
	
	@Column(name="name_user")
	private String nameUser;
	
	@Column(name="pass")
	private String pass;
	
	@Column(name="email")
	private String email;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="idUsuarioOrigen")
	@Cascade(CascadeType.ALL)
	private List<Publicacion> publicacionesList;
	
	
	@OneToMany 
	@JoinColumn(name="idUsuarioDestino")
	@Cascade(CascadeType.ALL)
	private List<Mensajes> mensajesList;
	
	@ManyToMany(mappedBy="usuariosLista", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	private List<Grupo>grupos;
	
	public Users() {
		super();
	}

	public long getIdUsers() {
		return idUsers;
	}

	public void setIdUsers(long idUsers) {
		this.idUsers = idUsers;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Publicacion> getPublicacionesList() {
		return publicacionesList;
	}

	public void setPublicacionesList(List<Publicacion> publicacionesList) {
		this.publicacionesList = publicacionesList;
	}

	public List<Mensajes> getMensajesList() {
		return mensajesList;
	}

	public void setMensajesList(List<Mensajes> mensajesList) {
		this.mensajesList = mensajesList;
	}

	public List<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Users [idUsers=" + idUsers + ", nameUser=" + nameUser + ", pass=" + pass + ", email=" + email
				+ ", publicacionesList=" + publicacionesList + ", mensajesList=" + mensajesList + ", grupos=" + grupos
				+ "]";
	}

	
	
}
