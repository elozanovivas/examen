package es.unex.mydai.dao;

import java.util.List;

import es.unex.mydai.persistence.vo.Grupo;
import es.unex.mydai.persistence.vo.Hashtags;

public interface GrupoDAO extends GenericDAO<Grupo, Long> {
	List<Grupo> todosGrupos() ;
}
