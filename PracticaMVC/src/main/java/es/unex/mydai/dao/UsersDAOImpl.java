package es.unex.mydai.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import es.unex.mydai.persistence.vo.Users;
@Component
@Transactional
public class UsersDAOImpl extends GenericDAOImpl<Users, Long> implements UsersDAO {
	
	@PersistenceContext
	protected EntityManager entityManager;
	public UsersDAOImpl() {
		this.setEntityClass(Users.class);
	}
	public Users busqueda(String email) {
		Users user2=this.entityManager.createQuery("from Users where email='"+email+"'",Users.class).getSingleResult();
		return this.entityManager.find(Users.class,user2.getIdUsers());
		
	}
	public List<Users> todosUsuarios() {
		List<Users> u=  this.entityManager.createQuery("from Users",Users.class).getResultList();
		return u; 
	}
	
	public Users loguearse(Users u) {
		try{System.out.println("el usuario u es:"+u.getEmail()+"y su pass:"+u.getPass());
			Users user=this.entityManager.createQuery("from Users where email='"+u.getEmail()+"' and pass='"+u.getPass()+"'",Users.class).getSingleResult();
			System.out.println("el usuario es"+user.getEmail()+" y la contraseņa"+user.getPass());
			return user;
		}catch(Exception e){
			Users user=new Users();
			return user;
		}
	
		
	}

}
