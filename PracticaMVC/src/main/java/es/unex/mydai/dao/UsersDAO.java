package es.unex.mydai.dao;

import java.util.List;

import es.unex.mydai.persistence.vo.Users;

public interface UsersDAO extends GenericDAO<Users, Long> {
	Users busqueda(String user);
	Users loguearse(Users u);
	List <Users> todosUsuarios();
}

