package es.unex.mydai.dao;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Users;
import es.unex.mydai.persistence.vo.Visitas;
@Component
@Transactional
public class VisitasDAOImpl extends GenericDAOImpl<Visitas, Long> implements VisitasDAO {

	@PersistenceContext
	protected EntityManager entityManager;
	public VisitasDAOImpl() {
		this.setEntityClass(Visitas.class);
	}
	public ArrayList<Visitas> busqueda(long id) {
		ArrayList<Visitas> v= (ArrayList<Visitas>) this.entityManager.createQuery("from Visitas where id_usuario_visitado='"+id+"'",Visitas.class).getResultList();
		return v; 
	}
}
