package es.unex.mydai.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Users;

@Component
@Transactional
public class MensajesDAOImpl extends GenericDAOImpl<Mensajes, Long> implements MensajesDAO{
	@PersistenceContext
	protected EntityManager entityManager;
	public MensajesDAOImpl() {
		this.setEntityClass(Mensajes.class);
	}
	
	public List<Mensajes> todosMensajes() {
		List<Mensajes> m=  this.entityManager.createQuery("from Mensajes",Mensajes.class).getResultList();
		return m; 
	}

	public List<Mensajes> busqueda(long id) {
		List<Mensajes> m=  this.entityManager.createQuery("from Mensajes where idUsuarioDestino="+id,Mensajes.class).getResultList();
		return m; 
	}
}
