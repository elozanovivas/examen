package es.unex.mydai.dao;

import java.util.ArrayList;

import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Visitas;

public interface VisitasDAO extends GenericDAO<Visitas, Long> {
	ArrayList<Visitas> busqueda(long id) ;
}
