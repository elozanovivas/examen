package es.unex.mydai.dao;

import java.util.List;

import es.unex.mydai.persistence.vo.Mensajes;

public interface MensajesDAO extends GenericDAO<Mensajes, Long>{
	List <Mensajes> todosMensajes();
	List <Mensajes> busqueda(long id);

}

