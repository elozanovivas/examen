package es.unex.mydai.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.mydai.persistence.vo.Grupo;
import es.unex.mydai.persistence.vo.Hashtags;
@Component
@Transactional
public class GrupoDAOImpl extends GenericDAOImpl<Grupo, Long> implements GrupoDAO {

	@PersistenceContext
	protected EntityManager entityManager;
	public GrupoDAOImpl() {
		this.setEntityClass(Grupo.class);
	}
	


	public List<Grupo> todosGrupos() {
		List<Grupo> a=  this.entityManager.createQuery("from Grupo",Grupo.class).getResultList();
		return a; 
	}
	
}
