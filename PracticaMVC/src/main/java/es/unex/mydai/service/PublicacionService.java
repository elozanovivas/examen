package es.unex.mydai.service;


import java.util.List;

import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Publicacion;

public interface PublicacionService {
	
	public Publicacion createPublicacion(Publicacion publicacion);
	
	public Publicacion findPublicacionById(long id);
	
	public Publicacion updatePublicacion(Publicacion publicacion);
	
	public void deletePublicacion(Publicacion publicacion);
	
	public List<Publicacion> todasPublicaciones();
//	
//	public List<Publicacion> busqueda(long id);
	
}
