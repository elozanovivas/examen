package es.unex.mydai.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.unex.mydai.dao.GrupoDAO;

import es.unex.mydai.persistence.vo.Grupo;
import es.unex.mydai.persistence.vo.Hashtags;
@Component
public class GrupoServiceImpl implements GrupoService {

	@Autowired
	private GrupoDAO grupoDAO;

	public Grupo createGrupo(Grupo grupo) {
		return grupoDAO.create(grupo);
	}

	public Grupo findGrupoById(long id) {
		return grupoDAO.read(id);
	}

	public Grupo updateGrupo(Grupo grupo) {
		return grupoDAO.update(grupo);
	}

	public void deleteGrupo(Grupo grupo) {
		grupoDAO.delete(grupo);
	}
	public List<Grupo> todosGrupos() {
		return grupoDAO.todosGrupos();
	}

}
