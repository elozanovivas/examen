package es.unex.mydai.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import es.unex.mydai.dao.HashtagDAO;
import es.unex.mydai.persistence.vo.Hashtags;

@Component
public class HashtagServiceImpl implements HashtagService {

	@Autowired
	private HashtagDAO hashtagDAO;

	public Hashtags createHashtag(Hashtags hashtag) {
		return hashtagDAO.create(hashtag);
	}

	public Hashtags findById(long id) {
		return hashtagDAO.read(id);
	}

	public Hashtags updateHashtag(Hashtags hashtag) {
		return hashtagDAO.update(hashtag);
	}

	public void deleteHashtag(Hashtags hashtag) {
		hashtagDAO.delete(hashtag);
	}
	public ArrayList<Hashtags> busqueda(long id) {
		return hashtagDAO.busqueda(id);
	}
	public List<Hashtags> todosHashtags() {
		return hashtagDAO.todosHashtags();
	}
}
