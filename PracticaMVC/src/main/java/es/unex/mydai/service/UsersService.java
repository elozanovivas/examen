package es.unex.mydai.service;



import java.util.List;

import es.unex.mydai.persistence.vo.Users;

public interface UsersService {
	
	public Users createUser(Users user);
	
	public Users findUserById(long id);
	
	public Users updateUser(Users user);
	
	public void deleteUser(Users user);
	
	public Users findUserbyname(String name);

	public Users loguearse(Users u);
	
	public List<Users> todosUsuarios();
	
}
