package es.unex.mydai.service;


import java.util.List;

import es.unex.mydai.persistence.vo.Mensajes;

public interface MensajesService {
	
	public Mensajes createMensaje(Mensajes mensajes);
	
	public Mensajes findMensajeById(long id);
	
	public Mensajes updateMensaje(Mensajes mensajes);
	
	public void deleteMensaje(Mensajes mensajes);
	
	public List<Mensajes> todosMensajes();
	
	public List<Mensajes> busqueda(long id);
	
}
