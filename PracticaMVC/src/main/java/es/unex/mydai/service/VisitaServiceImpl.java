package es.unex.mydai.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.unex.mydai.dao.VisitasDAO;
import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Visitas;

@Component
public class VisitaServiceImpl implements VisitaService {

	@Autowired
	private VisitasDAO visitasDAO;

	public Visitas createVisita(Visitas visitas) {
		return visitasDAO.create(visitas);
	}

	public Visitas findById(long id) {
		return visitasDAO.read(id);
	}

	public Visitas updateVisita(Visitas visitas) {
		return visitasDAO.update(visitas);
	}

	public void deleteVisita(Visitas visitas) {
		visitasDAO.delete(visitas);
	}
	public ArrayList<Visitas> busqueda(long id) {
		return visitasDAO.busqueda(id);
	}

}
