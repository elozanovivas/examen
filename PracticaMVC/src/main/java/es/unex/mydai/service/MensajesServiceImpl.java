package es.unex.mydai.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import es.unex.mydai.dao.MensajesDAO;
import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Mensajes;
@Component
public class MensajesServiceImpl implements MensajesService {

	@Autowired
	private MensajesDAO mensajesDAO;
	
	public Mensajes createMensaje(Mensajes mensaje) {
		return mensajesDAO.create(mensaje);
	}

	public Mensajes findMensajeById(long id) {
		return mensajesDAO.read(id);
	}

	public Mensajes updateMensaje(Mensajes mensaje) {
		return mensajesDAO.update(mensaje);
	}

	public void deleteMensaje(Mensajes mensaje) {
		mensajesDAO.delete(mensaje);
	}
	public List<Mensajes> todosMensajes() {
		return mensajesDAO.todosMensajes();
	}
	public List<Mensajes> busqueda(long id) {
		return mensajesDAO.busqueda(id);
	}
	
}
