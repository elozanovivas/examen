package es.unex.mydai.service;


import java.util.List;

import es.unex.mydai.persistence.vo.Grupo;

public interface GrupoService {
	
	public Grupo createGrupo(Grupo grupo);
	
	public Grupo findGrupoById(long id);
	
	public Grupo updateGrupo(Grupo grupo);
	
	public void deleteGrupo(Grupo grupo);
	
	public List<Grupo>todosGrupos ();

	
}
