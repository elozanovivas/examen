<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>A�adir grupo</title>
</head>
<body>
<form:form modelAttribute="grupo" action="unirseGrupo">
		<fieldset>
			<legend> Editar grupo </legend>
			<label for="id_">Introduce el id del grupo a unirte:</label>
			<form:input path="idGrupo" type="text" name="idGrupo" id="idGrupo"
				size="30" maxlength="150" />
			<br />
		
		</fieldset>
		<br />
		<br />
		<input type="submit" name="A�adir nuevo grupo" value="A�adir nuevo grupo" />
		<input type="reset" name="Borrar Datos" value="Borrar Datos">
</form:form>	
</body>
</html>