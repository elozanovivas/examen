<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Crear grupo</title>
</head>
<body>
<form:form modelAttribute="g" action="anadirGrupo">
		<fieldset>
			<legend> Crear grupo </legend>
			<label for="nombreGrupo_">Nombre</label>
			<form:input path="nombreGrupo" type="text" name="nombreGrupo" id="nombreGrupo"
				size="30" maxlength="150" />
			<br />
		
		</fieldset>
		<br />
		<br />
		<input type="submit" name="Anadir grupo" value="A�adir grupo" />
		<input type="reset" name="Borrar Datos" value="Borrar Datos">
</form:form>	
</body>
</html>