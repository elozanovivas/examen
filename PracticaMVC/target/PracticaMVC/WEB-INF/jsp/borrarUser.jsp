<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Seguro que desea eliminar este usuario</title>
</head>
<body>
	<form:form modelAttribute="user" action="borrarUser">
		<fieldset>
			<legend> Editar Usuario </legend>
			<label for="emai">Email</label>
			<form:input path="email" type="text" name="email" id="email" size="30" maxlength="150" />
			<label for="nameUse">Usuario</label>
			<form:input path="nameUser" type="text" name="nameUser" id="nameUser"
				size="30" maxlength="150" />
			<br /> <label for="pass">Contraseņa</label>
			<form:input path="pass" type="password" name="pass" id="pass"
				size="30" maxlength="250" />
		</fieldset>
		<br />
		<br />
		<input type="submit" name="Eliminar" value="Eliminar" />

	</form:form>

</body>
</html>