<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="es.unex.mydai.persistence.vo.Users"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Crear un nuevo usuario</title>
</head>
<body>
<form:form modelAttribute="u" action="anadirUsuario">
		<fieldset>
			<legend> Crear Usuario </legend>
						<label for="email">Email</label>
			<form:input path="email" type="text" name="email" id="email"
				size="30" maxlength="150" />
			<label for="nameUser">Nombre</label>
			<form:input path="nameUser" type="text" name="nameUser" id="nameUser"
				size="30" maxlength="150" />
<%-- 			<label for="surnameUser">Apellidos</label>
			<form:input path="surnameUser" type="text" name="surnameUser" id="surnameUser"
				size="30" maxlength="150" /> --%>
			<br />
			<label for="pass">Contraseņa</label>
			<form:input path="pass" type="password" name="pass"
				id="pass" size="30" maxlength="250" />
		</fieldset>
		<br />
		<br />
		<h2>${mensaje}</h2>
		<input type="submit" name="Crear Usuario" value="Crear Usuario" />
		<input type="reset" name="Borrar Datos" value="Borrar Datos">
		
	</form:form>
	
</body>
</html>